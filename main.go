package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
)

func question1() {
	var num int
	odds := make([]int, 0)

	for {
		num++

		if num%2 == 1 {
			odds = append(odds, num)
		}

		if len(odds) == 100 {
			break
		}
	}

	fmt.Println(len(odds), "bilangan ganjil pertama :", odds)
}

func primeFactors(num int) []int {
	primeFactors := make([]int, 0)

	for num%2 == 0 {
		primeFactors = append(primeFactors, 2)
		num = num / 2
	}

	for i := 3; i*i <= num; i = i + 2 {
		for num%i == 0 {
			primeFactors = append(primeFactors, i)
			num = num / i
		}
	}

	if num > 2 {
		primeFactors = append(primeFactors, num)
	}

	return primeFactors
}

func question2() {
	var num int
	primes := make([]int, 0)

	for {
		num++

		if len(primeFactors(num)) == 1 {
			primes = append(primes, num)
		}

		if len(primes) == 100 {
			break
		}
	}

	fmt.Println(len(primes), "bilangan prima pertama :", primes)
}

func question3(num int) {
	factorials := make([]string, 0)

	for i := num; i > 0; i-- {
		if i == 1 {
			factorials = append(factorials, strconv.Itoa(i))
		} else {
			factorials = append(factorials, strconv.Itoa(i)+"x")
		}
	}

	fmt.Println("output :", strings.Join(factorials, ""))
}

func question4() {
	var n, counter, total float64

	fmt.Print("input number : ")
	fmt.Scan(&n)

	if n <= 0 {
		log.Println("input must be higher than 0")
		return
	}

	for {
		counter++

		total += 1 / (math.Sqrt(counter) + (math.Sqrt((counter + 1))))

		if counter == n {
			break
		}
	}

	fmt.Println("jumlah deret : ", total)
}

func question5() {
	var n, counter, totalInterestRate float64

	money := float64(100)

	fmt.Print("input tahun : ")
	fmt.Scan(&n)

	for {
		counter++

		totalInterestRate += (0.5 / 100) * money
		money += totalInterestRate

		if counter == n {
			break
		}
	}
	fmt.Println("jumlah uang brandon : USD", money)
}

func question6(word, sentence string) bool {
	var counter int

	for i := 0; i < len(word); i++ {
		for j := 0; j < len(sentence); j++ {
			if word[i] == sentence[j] {
				counter++
				break
			}
		}
	}

	return counter == len(word)
}

func main() {
	fmt.Println("1. Tulis program untuk menampilkan 100 bilangan Ganjil pertama \nanswer :")
	question1()
	fmt.Println(`
		===================================================================================
	`)
	fmt.Println("2. Tulis program untuk menampilkan 100 bilangan Prima pertama \nanswer :")
	question2()
	fmt.Println(`
		===================================================================================
	`)
	fmt.Println("3. Tulis Program untuk menghitung angka factorial ( misal 5! ) \nanswer :")
	question3(5)
	fmt.Println(`
		===================================================================================
	`)
	fmt.Println("4. Buatkan sebuah fungsi untuk menghitung jumlah dari deret di bawah ini \nanswer :")
	question4()
	fmt.Println(`
		===================================================================================
	`)
	fmt.Println("5. Soal Cerita: Brandon menabung sejumlah uang sebesar USD 100 di Bank Digital dengan interest rate sebesar 0.5% per tahun. Berapa jumlah uang yang dimiliki Brandon pada tahun ke n? \nanswer :")
	question5()
	fmt.Println(`
		===================================================================================
	`)
	fmt.Println("6. Buatkan sebuah fungsi untuk melakukan pencarian sebuah kata apakah terdapat di dalam sebuah kalimat.")
	fmt.Println("(dead,", "red dead redemption) ->", question6("dead", "red dead redemption"))
	fmt.Println("(de,", "red dead redemption) ->", question6("de", "red dead redemption"))
	fmt.Println("(homer,", "red dead redemption) ->", question6("homer", "red dead redemption"))
}
