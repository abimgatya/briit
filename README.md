## Technical Test BRIIT

### SQL QUESTION

1. Total amount transaksi terminal SV0003
   ```sh
    select sum(amount) as total_amount from tb_transaksiatm where terminal = 'SV0003';
   ```
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/sql/sql1.png" alt="sql1">

2. Total amount transaksi kartu Bank C
   ```sh
    select sum(tr.amount) as total_amount from tb_issuer as i
    right join tb_transaksiatm as tr on left(tr.nomor_kartu, 4) = i.prefix
    where i.bank = 'C';
   ```
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/sql/sql2.png" alt="sql1">

3. Tampilkan report dengan kolom sebagai berikut :
    <br><img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/sql/table.png" alt="sql1">
   ```sh
    select te.merk, count(tr.terminal) as jumlah_transaksi, sum(amount) as total_amount
    from tb_terminalatm as te
    right join tb_transaksiatm as tr on tr.terminal = te.nomor_terminal
    group by te.merk
    order by te.merk asc;
   ```
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/sql/sql3.png" alt="sql1">


### PSEUDOCODE QUESTION

1. Tulis program untuk menampilkan 100 bilangan Ganjil pertama
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode1.png" alt="sql1">

2. Tulis program untuk menampilkan 100 bilangan Prima pertama
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode2.png" alt="sql1">

3. Tulis Program untuk menghitung angka factorial ( misal 5! )
    Input 5, maka output : 5x4x3x2x1
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode3.png" alt="sql1">

4. Buatkan sebuah fungsi untuk menghitung jumlah dari deret di bawah ini :
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/question4.png" alt="sql1"><br>
    Dengan input n > 0
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode4.png" alt="sql1">

5. Soal Cerita: Brandon menabung sejumlah uang sebesar USD 100 di Bank Digital dengan
interest rate sebesar 0.5% per tahun. Berapa jumlah uang yang dimiliki Brandon pada tahun
ke n?
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode5.png" alt="sql1">

6. Buatkan sebuah fungsi untuk melakukan pencarian sebuah kata apakah terdapat di dalam
sebuah kalimat.
Contoh :
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/question6.png" alt="sql1"><br>
    Dengan input n > 0
    <img src="https://gitlab.com/abimgatya/briit/-/raw/master/img/pseudocode/pseudocode6.png" alt="sql1">